steps = [
		 [
		 #create table account
		 """
		 CREATE TABLE vacations (
		id SERIAL PRIMARY KEY NOT NULL,
		name VARCHAR(100) NOT NULL,
		from_date DATE NOT NULL,
		to_date DATE NOT NULL,
        thoughts TEXT
		);
		""",
		#drop the table
		"""
		DROP TABLE vacations;
		"""
		 ]
]