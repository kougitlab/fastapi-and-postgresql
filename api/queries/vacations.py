from pydantic import BaseModel
from typing import Optional, List, Union
#the import optional allows us to leave a filed as optional
from datetime import date
from queries.pool import pool

class Error(BaseModel):
    message: str
    #this error models helps us with our model

class VacationIn(BaseModel):
    name: str
    from_date: date
    to_date: date
    thoughts: Optional[str]
    
class VacationOut(BaseModel):
    id: int
    name: str
    from_date: date
    to_date: date
    thoughts: Optional[str]
    
class VacationRepository:
    
    def get_one(self, vacation_id: int) -> Optional[VacationOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , name
                            , from_date
                            , to_date
                            , thoughts
                        FROM vacations
                        WHERE id = %s
                        """,
                        [vacation_id]
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_vacation_out(record)
        except Exception as e:
            return{"message": "Could not get one, issue with get_one code"}
            
    def delete(self, vacation_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        DELETE FROM vacations
                        where id = %s
                        """,
                        [vacation_id]
                    )
                    return True
        except Exception as e:
            return{"message": "Could not delete, issue with delete code in def delete"}
    
    def update(self, vacation_id, vacation: VacationIn) -> Union[VacationOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        UPDATE vacations
                        SET name= %s
                            , from_date = %s
                            , to_date = %s
                            , thoughts = %s
                        WHERE id = %s
                        """,
                        [
                            vacation.name,
                            vacation.from_date,
                            vacation.to_date,
                            vacation.thoughts,
                            vacation_id
                        ]
                    )
                    #order is very important for update!
                    return self.vacation_in_to_out(vacation_id, vacation)
        except Exception as e:
            print(e)
            return{"message": "Could not get update vacations, issue with def update in queries vacations.py"}
    
    def get_all(self) -> Union[Error, List[VacationOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, name, from_date, to_date, thoughts
                        FROM vacations
                        ORDER by from_date;
                        """
                    )
                    result = []
                    for record in db:
                        vacation = VacationOut(
                            id= record[0],
                            name= record[1],
                            from_date= record[2],
                            to_date= record[3],
                            thoughts= record[4],
                        )
                        #in the for loop above, django did those foryou
                        result.append(vacation)
                    return result
        except Exception as e:
            print(e)
            return{"message": "Could not get all vacations error def get all in queries vacations.py"}
    
    def create(self, vacation: VacationIn) -> VacationOut:
        
        try:
        #make sure to add self,
        #connect database, will create pool of connection...with is a monitor, avoids tri catch blocks
            with pool.connection() as conn:
                #get a cursor(something to run SQL with)
                with conn.cursor() as db:
                    #run our insert statement
                    result = db.execute(
                        """
                        INSERT INTO vacations
                            (name, from_date, to_date, thoughts)
                        VALUES
                            (%s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [vacation.name, vacation.from_date, vacation.to_date, vacation.thoughts]
                    )
                    #return new data
                    
                    #returning id, returns the id
                    id = result.fetchone()[0]
                    
                    # old_data = vacation.dict()
                    # return VacationOut(id=id, **old_data)
                    return self.vacation_in_to_out(id, vacation)
        except Exception as e:
            return{"message": "Could not get all vacations error def get all in queries vacations.py"}
            
            #if just copying data from old pydantic, can use splat and old data.
                #you can find documentation for psycopg in documentation.
    def vacation_in_to_out(self, id: int, vacation: VacationIn):
        old_data = vacation.dict()
        return VacationOut(id=id, **old_data)
    #the 2 lines originally from the top, from create vacation, but we put it down here.
            
    def record_to_vacation_out(self, record):
        return VacationOut(
            id=record[0],
            name=record[1],
            from_date=record[2],
            to_date=record[3],
            thoughts=record[4],
        )
                